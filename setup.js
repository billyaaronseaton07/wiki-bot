const prompt = require('prompt');
const editDotenv = require('edit-dotenv');
const fs = require('fs');

prompt.start();

prompt.message = ""
prompt.delimiter = ""

prompt.get({
    properties: {
        botsite: {
            description: "Enter your the mediawiki site that you wish for the bot to act on",
            required: true
        },
        botusername: {
          description: "Enter your bot's username",
          required: true
        },
        botpassword: {
            description: "Enter your bot's password (input hidden)",
            hidden: true,
            required: true
        }
      }
    }, function (err, result) {
    const dotenvchanges = {
        MEDIAWIKISITE: result.botsite,
        BOTUSERNAME: result.botusername,
        BOTPASSWORD: result.botpassword
    };
    const newdotenv = editDotenv(``, dotenvchanges);
    fs.unlink('.env', (err) => {
        if (err) throw err;
    });
    fs.writeFile('.env', newdotenv, (err) => {
        if (err) throw err;
    });
});